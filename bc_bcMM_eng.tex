\documentclass[final, 12pt,  notitlepage]{revtex4-1}

\usepackage[hidelinks]{hyperref}
\usepackage{booktabs}
\usepackage{amsmath}
\usepackage{braket}

\renewcommand{\arraystretch}{1.2}

\newcommand{\Br}{\mathrm{Br}}
\newcommand{\B}{\mathcal{B}}
\begin{document}

\title{Lepton pair production in radiative $B_{c}$ meson decays}
\author{A. V. Berezhnoy}
\author{A. K. Likhoded}
\author{A. V. Luchinsky}
\maketitle{}

The branching fraction of lepton pair production in radiative decays of the excited $B_{c}$ meson $B_{1}\to\B_{2}\ell\ell$ can be written in the form \cite{Faessler:1999de,Luchinsky:2017pby}
\begin{align}
  \label{eq:br_MM}
  \frac{d\Br_{\ell\ell}}{dq^{2}} &= \frac{\alpha}{3\pi}\frac{1}{q^{2}}\frac{\lambda(M_{1};M_{2},\sqrt{q^{2}})}{\lambda(M_{1};M_{2},0)}\left(1-\frac{2m_{\ell}^{2}}{q^{2}}\right)\sqrt{1-\frac{4m_{\ell}^{2}}{q^{2}}}\Br_{\gamma}=\frac{dI^{\ell\ell}(q^{2})}{dq^{2}}\Br_{\gamma}
\end{align}
where $\Br_{\gamma}$ is the branching fraction of the original radiative decay, $q^{2}$ is the squared invariant mass of the lepton pair, $\alpha=e^{2}/4\pi$ is the fine structure coupling constant and, $M_{1,2}$ are the masses of initial and final mesons respectively, and
\begin{align}
  \label{eq:lambda}
  \lambda(M;m_{1},m_{2}) &= \sqrt{1-\frac{(m_{1}+m_{2})^{2}}{M_{2}}} \sqrt{1-\frac{(m_{1}-m_{2})^{2}}{M_{2}}}
\end{align}
is the velocity of the final particles in $M\to m_{1}m_{2}$ decay.

It should be noted that the relation \eqref{eq:br_MM} is universal and does not depend on the physics of the process. The only assumption that was made is that we neglect the $q^{2}$ dependence of the $B_{1}\to B_{2}\gamma^{*}$ decay vertex. This assumption looks quit reasonable since typilcal energy deposit in the radiative decays of the doubly heavy mesons is small in comparison with quarks' masses. As a result, the converion factor $I^{\ell\ell}$ depends only on the masses of the initial and final particles. Masses of the leptons and ground state $B_{c}$ meson can be found easily \cite{Tanabashi:2018oca}, while for the initial excited particles some theoretical models are required.

Let us first discuss the quantum numbers of these particles. According to potential models, $B_{c}$ mesons in the valence approximation are build from $b$ and $c$ quarks. If we label orbital momentum of this pair and its spin as $L$ and $S$ respectively, then the total spin of the particle $J$ can has the values $|L-S|\le J \le L+S$, while its space parity is $P=(-1)^{L+1}$. As a result, observed experimentally ground state can be asigned the numbers $L=S=J=0$, or in ${}^{2S+1}L_{J}$ notation ${}^{3}S_{0}$. It is clear, that in radiative decay eithe spin of quark-antiquark pair or its orbital momentum should be changed by unity (so called M1 and E1 transiotions respectively), so either ${}^{3}S_{1}$ ($S=1$, $L=1$, $J=1$) or ${}^{1}P_{1}$ ($L=1$, $S=0$, $J=1$) mesons could be initial particles in the considered decays. The first option corresponds to $B_{c}^{*}$ meson, while the second one is a little bit more interesting. Since the $C$ parity in the sector of $B_{c}$ mesons is not defined, spin-orbital interation can lead to the mixing of ${}3P_{1}$ and ${}^{1}P_{1}$ states, giving two physical mesons,
\begin{align}
  \label{eq:mix1}
  \ket{1^{+}} &= \cos\theta_{P} \ket{{}^{1}P_{1}} + \sin\theta_{P}\ket{{}^{3}P_{1}'}, \quad
  \ket{1^{+'}} = \sin\theta_{P} \ket{{}^{1}P_{1}} - \cos\theta_{P}\ket{{}^{3}P_{1}'} ,
\end{align}
where $\theta_{P}$ is the mixing angle. As a result, both $\ket{q^{+}}$ and $1^{+'}$ can decay into the ground state. In the following we will present numerical values of the $\gamma^{*}\to\ell\ell$ conversion factors for all the initial particles.

The spectroscopy of $B_{c}$ mesons was studied already in details. In our work we will use results presented in papers \cite{Godfrey:1985xj, Ebert:2002pp, Fulcher:1998ka, Kiselev:1994rc, Eichten:1994gt, Gupta:1995ps, Zeng:1994vj} (see also \cite{Godfrey:2004ya}). The mass of $B_{c}^{*}$ meson is not high enough for muon pair production, so only the $ee$ channel is opened. The corresponding results are shown in table \ref{tab:tab1}. As for $P$ wave escitations, both electronic and muonic decays are allowed and the integrated conversion factors are presented in tables \ref{tab:tab2}, \ref{tab:tab3}. As you can see, in all cases electron pair emission leads to suppression of the branching fraction by a factor $\sim 10^{-2}$, while in the case of $\mu\mu$ channel the suppresion is about an order of magnitude stronger. It shold be noticed, however, that even after such a suppression the branching fractions of the decays are still noticable. Since, unlike soft photon, the lepton-antilepton pair can be easily detected by the modern detectors (LHCb, etc) we think that the exited $B_{c}$ mesons could be obseved in discussed in our paper modes.




\begin{table}
  \centering
\begin{tabular}{c|ccccccc}
\hline
 & GI \cite{Godfrey:1985xj}  & EFG \cite{Ebert:2002pp}  & FUII \cite{Fulcher:1998ka}  & GKLT \cite{Kiselev:1994rc}  & EQ \cite{Eichten:1994gt}  & GJ \cite{Gupta:1995ps}  & ZVR \cite{Zeng:1994vj} \\ 
\hline
 $M$  & $6.338$  & $6.332$  & $6.341$  & $6.317$  & $6.337$  & $6.308$  & $6.34$ \\ 
 $10^{3} I^{ee}$  & $6.105$  & $5.616$  & $5.431$  & $5.665$  & $5.869$  & $5.591$  & $6.011$ \\ 
\hline
\end{tabular}
  \caption{Conversion factors for $B_c^*\to B_c\gamma$ decay}
  \label{tab:tab1}
\end{table}

\begin{table}
  \centering
\begin{tabular}{c|ccccccc}
\hline
 & GI \cite{Godfrey:1985xj}  & EFG \cite{Ebert:2002pp}  & FUII \cite{Fulcher:1998ka}  & GKLT \cite{Kiselev:1994rc}  & EQ \cite{Eichten:1994gt}  & GJ \cite{Gupta:1995ps}  & ZVR \cite{Zeng:1994vj} \\ 
\hline
 $M$  & $6.741$  & $6.734$  & $6.737$  & $6.717$  & $6.73$  & $6.738$  & $6.73$ \\ 
 $10^{3} I^{ee}$  & $8.811$  & $8.733$  & $8.689$  & $8.733$  & $8.74$  & $8.821$  & $8.753$ \\ 
 $10^{3} I^{\mu\mu}$  & $0.7192$  & $0.6538$  & $0.6176$  & $0.6538$  & $0.6593$  & $0.7272$  & $0.6703$ \\ 
\hline
\end{tabular}
  \caption{Conversion factors for $B_c(1P_{1})\to B_c\gamma$ decay}
  \label{tab:tab2}
\end{table}

\begin{table}
  \centering
\begin{tabular}{@{}llllllll@{}}
\hline%\toprule
 & GI \cite{Godfrey:1985xj}  & EFG \cite{Ebert:2002pp}  & FUII \cite{Fulcher:1998ka}  & GKLT \cite{Kiselev:1994rc}  & EQ \cite{Eichten:1994gt}  & GJ \cite{Gupta:1995ps}  & ZVR \cite{Zeng:1994vj} \\ 
\hline% \midrule
 $M$  & $6.75$  & $6.749$  & $6.76$  & $6.729$  & $6.736$  & $6.757$  & $6.74$ \\ 
 $10^{3} I^{ee}$  & $8.84$  & $8.782$  & $8.766$  & $8.773$  & $8.76$  & $8.88$  & $8.786$ \\ 
 $10^{3} I^{\mu\mu}$  & $0.7432$  & $0.6949$  & $0.6813$  & $0.6867$  & $0.6758$  & $0.7774$  & $0.6976$ \\ 
\hline%\bottomrule
\end{tabular}
  \caption{Conversion factors for $B_c(1P_{1}')\to B_c\gamma$ decay}
  \label{tab:tab3}
\end{table}

\bibliographystyle{apsrev4-1}
\bibliography{bc_bcMM_litr}

\end{document}
